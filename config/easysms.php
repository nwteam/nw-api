<?php
return [
    // HTTP 请求的超时时间（秒）
    'timeout' => 10.0,

    // 默认发送配置
    'default' => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

        // 默认可用的发送网关
        'gateways' => [
            'twilio',
        ],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => '/tmp/easy-sms.log',
        ],
        'twilio' => [
            'account_sid' => env('TWILIO_SID'), // sid
            'from' => env('TWILIO_NUMBER'), // 发送的号码 可以在控制台购买
            'token' => env('TWILIO_TOKEN'), // apitoken
        ],
    ],
];
